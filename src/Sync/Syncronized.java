package Sync;

public class Syncronized implements Runnable {
    private Integer id, que;
    private Contador c = new Contador();

    public Syncronized(int id, int que, Contador c) {
        this.id = id;
        this.que = que;
        this.c = c;
    }

    public void run(){   //  try/catch ...
        System.out.println("comienza hilo (" + id.toString() + ")");
        if (que == 1) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("empiezo wait(" + id.toString() + ")");
            try {
                c.esperar();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("salgo wait(" + id.toString() + ")");
        } else {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("disparo notify(" + id.toString() + ")");
            c.notificar();
            System.out.println("fin disparo notify(" + id.toString() + ")");
        }
        System.out.println("fin run(" + id.toString() + ")");
    }
}