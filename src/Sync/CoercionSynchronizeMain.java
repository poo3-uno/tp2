package Sync;

public class CoercionSynchronizeMain {
    public static void main(String[] args) {
            Contador c1= new Contador();
            Syncronized p1=new Syncronized(1, 1, c1);
            Thread t1=new Thread(p1);
            Thread t2=new Thread(new Syncronized(2, 1, c1));
            Thread t3=new Thread(new Syncronized(3, 1, c1));
            Thread t4=new Thread(new Syncronized(4, 2, c1));
            Thread t5=new Thread(new Syncronized(5, 2, c1));
            t1.start();
            t2.start();
            t3.start();
            t4.start();
            t5.start();

    }
}
