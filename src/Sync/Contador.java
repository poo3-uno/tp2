package Sync;

public class Contador {

    Integer c;

    public Contador() {
        c = 0;
    }

    public synchronized void esperar() throws InterruptedException {
        wait(10000);
    }

    public synchronized void notificar() {
        notify();
    }  //notifyAll

}
