package performance;

import java.util.ArrayList;

public class EjercicioVectorMain {

    public static void main(String[] args) throws InterruptedException {
        //// Version monohilo


        Integer repeticiones=10000;
        System.out.println(" COMIENZO DE EJECUCION DE "+repeticiones+" FUNCIONES DE CREACION Y ORDENAMIENTO DE VECTOR");
        System.out.println(" VECTORES: ");

       long inicio = System.currentTimeMillis();

        int i;
        for(i=0;i<repeticiones;i++){
            Vector vector = new Vector();
            vector.createAndOrderVector();
        }

        long fin = System.currentTimeMillis();

        double tiempo = (double) ((fin - inicio)/1000);

        System.out.println(tiempo +" segundos sin manejo de hilos");

        //Version multithread

        ArrayList<Thread> listThread = new ArrayList<>();

        for(int j=0;j<repeticiones;j++){
            Vector vectorThread = new Vector();
            Thread thread1 = new Thread(vectorThread);
            listThread.add(thread1);
        }
        long inicioMultiThread = System.currentTimeMillis();

        for(int j=0;j<listThread.size();j++){
            listThread.get(j).start();
        }

        for(int j=0;j<listThread.size();j++){
            listThread.get(j).join();
        }

        long finMultiThread = System.currentTimeMillis();
        double tiempoMultiThread = (double) ((finMultiThread - inicioMultiThread)/1000);
        System.out.println(tiempoMultiThread +" segundos con MultiThread");

    }
}
