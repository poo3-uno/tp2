package performance;

import java.util.ArrayList;
import java.util.Collections;

public class Vector implements Runnable{

    public synchronized void esperar() throws InterruptedException {
        wait();
    }

    public synchronized void notificar() {
        notify();
    }

    public static void createAndOrderVector() {
        Integer posiciones=10000;
        Integer hasta=10;
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int i=0; i<posiciones;i++){
            int value = (int) Math.floor(Math.random()*hasta+1);
            list.add(value);
        }
        Collections.sort(list);
    }



    @Override
    public void run() {
        createAndOrderVector();
    }


}
