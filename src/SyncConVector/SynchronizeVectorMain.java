package SyncConVector;

public class SynchronizeVectorMain {
    public static void main(String[] args) {
            VectorSincronizado c1= new VectorSincronizado();
            SyncronizedVector p1=new SyncronizedVector( 1, c1);
            Thread t1=new Thread(p1);
            Thread t2=new Thread(new SyncronizedVector( 1, c1));
            Thread t3=new Thread(new SyncronizedVector( 1, c1));
            Thread t4=new Thread(new SyncronizedVector( 2, c1));
            Thread t5=new Thread(new SyncronizedVector( 2, c1));
            t1.start();
            t2.start();
            t3.start();
            t4.start();
            t5.start();

    }
}
