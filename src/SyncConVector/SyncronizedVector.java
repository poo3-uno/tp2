package SyncConVector;

public class SyncronizedVector implements Runnable {
    private int que;
    private VectorSincronizado c = new VectorSincronizado();

    public SyncronizedVector( int que, VectorSincronizado c) {
        this.que=que;
        this.c = c;
    }

    public void run(){
        c.createAndOrderVector();
        if (que == 1) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            try {
                c.esperar();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } else {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            c.notificar();
        }
    }
}