package SyncConVector;

import java.util.ArrayList;
import java.util.Collections;

public class VectorSincronizado {



    public VectorSincronizado() {

    }

    public synchronized void esperar() throws InterruptedException {
        wait();
    }

    public synchronized void notificar() {
        notify();
    }  //notifyAll

    public static void createAndOrderVector() {
        Integer posiciones=50;
        Integer hasta=10;
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int i=0; i<posiciones;i++){
            int value = (int) Math.floor(Math.random()*hasta+1);
            list.add(value);
        }
        Collections.sort(list);
        System.out.println(list+Thread.currentThread().getName());
    }

}
