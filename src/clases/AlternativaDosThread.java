package clases;

import static java.lang.Thread.sleep;

public class AlternativaDosThread implements Runnable{

    private Integer id;

    public AlternativaDosThread(Integer id) {
        this.id = id;
    }

    @Override
    public void run(){
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Hola, soy " + id.toString());

    }
}
