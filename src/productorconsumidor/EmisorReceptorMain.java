package productorconsumidor;

public class EmisorReceptorMain {
    public static void main(String[] args) {

        Informacion informacion = new Informacion();
        String mensaje[] = {
                "SOY",
                "MENSAJE",
                "1",
                "Fin del Mensaje"
        };
        String mensaje2[] = {
                "SOY",
                "MENSAJE",
                "2",
                "Fin del Mensaje"
        };

        String mensajeCodificado[] = informacion.codificar(mensaje).toArray(new String[0]);
        String mensajeCodificado2[] = informacion.codificar(mensaje2).toArray(new String[0]);

        Thread sender = new Thread(new Emisor(informacion, mensajeCodificado));
        Thread receiver = new Thread(new Receptor(informacion));
        Thread sender2 = new Thread(new Emisor(informacion, mensajeCodificado2));
        Thread receiver2 = new Thread(new Receptor(informacion));

        sender.start();
        receiver.start();
        sender2.start();
        receiver2.start();

    }
}
