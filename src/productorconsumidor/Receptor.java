package productorconsumidor;

import java.util.concurrent.ThreadLocalRandom;

public class Receptor implements Runnable{

    private Informacion informacion;

    public Receptor(Informacion informacion) {
        this.informacion= informacion;
    }

    // standard constructors

    public void run() {
        for(String receivedMessage = informacion.recibirInformacion();
            !"Fin del Mensaje".equals(receivedMessage);
            receivedMessage = informacion.recibirInformacion()) {

            receivedMessage = informacion.decodificar(receivedMessage);
            System.out.println("Recibo y decodifico dato : "+receivedMessage);


            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
