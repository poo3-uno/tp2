package productorconsumidor;

import java.util.concurrent.ThreadLocalRandom;

public class Emisor implements Runnable{
    private Informacion informacion;
    private String[] datos;

    public Emisor(Informacion informacion, String[] mensaje) {
        this.informacion = informacion;
        this.datos = mensaje;
    }


    public void run() {


        for (String dato : datos) {
            System.out.println("Envio dato codificado : " + dato);
            informacion.enviarInformacion(dato);

            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
