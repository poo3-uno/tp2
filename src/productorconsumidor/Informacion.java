package productorconsumidor;

import java.util.ArrayList;
import java.util.Base64;

public class Informacion {

    private String datos;

    /*
    * Verdadero = receptor espera
    * Falso = emisor espera
    *
    * */
    private boolean transfiriendo = true;

    public synchronized String recibirInformacion() {
        while (transfiriendo) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Fallo de hilo");
            }
        }
        transfiriendo = true;

        String returnDatos = datos;
        notifyAll();
        return returnDatos;
    }

    public synchronized void enviarInformacion(String datos) {
        while (!transfiriendo) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Fallo de Hilo");
            }
        }
        transfiriendo = false;

        this.datos = datos;
        notifyAll();
    }

    public ArrayList<String> codificar(String[] mensaje) {
       ArrayList<String> mensajeCodificado = new ArrayList();
       int i;

        for(i=0;i<mensaje.length;i++){
            String encodedString = Base64.getEncoder().encodeToString(mensaje[i].getBytes());
            mensajeCodificado.add(encodedString);
        }

        return mensajeCodificado;
    }

    public String decodificar(String receivedMessage) {

        byte[] decodedBytes = Base64.getDecoder().decode(receivedMessage);
        String decodedString = new String(decodedBytes);

        return decodedString;
    }
}
