package ejecucion;

import clases.AlternativaDosThread;
import clases.AlternativaUnoThread;

import java.util.ArrayList;

public class UsoAlternativaDosThread {



    public static void main(String[] args) throws InterruptedException {
       // AlternativaDosThread uno = new AlternativaDosThread(20);
       // AlternativaDosThread dos = new AlternativaDosThread(30);

        //Thread thread1 = new Thread(uno);
        //Thread thread2 = new Thread(dos);


        //thread1.start();
        //thread2.start();

        Integer cantProcess=1000000;
        ArrayList<Thread> listThread = new ArrayList<Thread>();

        for(int i=0;i<cantProcess;i++){
            AlternativaDosThread uno = new AlternativaDosThread(i);
            Thread thread1 = new Thread(uno);
            listThread.add(thread1);
            }

        for(int i=0;i<listThread.size();i++){
            listThread.get(i).start();
        }

        for(int i=0;i<listThread.size();i++){
            listThread.get(i).join();
        }


        System.out.println("Termine los " + cantProcess + " procesos exitosamente");

    }
}
