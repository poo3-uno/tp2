package ejercicioProductorConsumidorPPT3;

public class Consumidor implements Runnable {

    private Buffer buf = null;

    public Consumidor(Buffer buf) {
        this.buf = buf;
    }

    public void run() {
        double item;
        while (true) {
            item = buf.extraer();
            try {
                Thread.sleep(500);  // try/catch
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("Consumiendo " + item);
        }
    }
}
