package ejercicioProductorConsumidorPPT3;

public class Productor implements Runnable {
    private Buffer buf = null;

    public Productor(Buffer buf) {
        this.buf = buf;
    }

    public void run() {
        double item = 0.0;
        while (true) {  //try/catch
            try {
                Thread.sleep(500);
                buf.insertar(++item);
                System.out.println("Produciendo " + item);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }


    }
}
