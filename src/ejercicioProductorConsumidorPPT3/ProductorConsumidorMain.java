package ejercicioProductorConsumidorPPT3;

public class ProductorConsumidorMain {

    public static void main(String[] args) {
        Buffer monitor = new Buffer(10);
        Thread hilo1 = new Thread(new Productor(monitor));
        Thread hilo2 = new Thread(new Productor(monitor));
        Thread hilo3 = new Thread(new Productor(monitor));
        Thread hilo4 = new Thread(new Consumidor(monitor));
        Thread hilo5 = new Thread(new Consumidor(monitor));

        hilo1.start();
        hilo2.start();
        hilo3.start();
        hilo4.start();
        hilo5.start();
    }

}
